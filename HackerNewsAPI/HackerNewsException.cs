﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HackerNewsFeed.HackerNewsAPI
{
    public class HackerNewsException : Exception
    {
        public HackerNewsException(string message) : 
            base("Failed to reach Hacker news because: " + message)
        {
            
        }
    }
}
