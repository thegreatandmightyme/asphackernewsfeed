﻿using System;
using System.Runtime.Caching;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace HackerNewsFeed.HackerNewsAPI
{
    public class HackerNewsAPI
    {
        private static readonly string BASE_NEWS_URL = "https://hacker-news.firebaseio.com/v0/";
        private static readonly string NEW_NEWS_PATH = "newstories.json";
        private static readonly string ITEM_PATH = "item/";

        private static readonly HttpClient client = new HttpClient();

        private static readonly MemoryCache itemCache = new MemoryCache("ItemCache");
        private static readonly CacheItemPolicy itemCachePolicy = new CacheItemPolicy();
        static HackerNewsAPI() {
            itemCachePolicy.SlidingExpiration = TimeSpan.FromSeconds(20);
        }

        public static async Task<List<NewsOverview>> GetNewStories(
            int storiesPerPage = 20,
            int page = 0,
            string search = null)
        {
            //Just wait.  We can't move on without these
            List<long> itemIds = await GetTopStoryIds();

            int storiesPassed = 0;
            List<NewsOverview> result = new List<NewsOverview>();
            foreach (long itemId in itemIds)
            {
                //Cast is safe because only insert is 2 lines down and it will be an item
                Item item = (Item)itemCache.Get(itemId.ToString());
                if (item == null)
                {
                    item = await GetItemAsync(itemId);
                    itemCache.Add(itemId.ToString(), item, itemCachePolicy);
                }
                if (item != null && item.deleted == false && item.type == ItemType.story)
                {
                    //Pass on anything that violates the search condition
                    if (search != null && search.Length > 0 && !item.title.Contains(search))
                    {
                        continue;
                    }

                    //Skip stories until we get to the proper page
                    if (storiesPassed < page * storiesPerPage)
                    {
                        storiesPassed++;
                        continue;
                    }
                    result.Add(new NewsOverview(item.title, item.by, item.url));
                    if (result.Count >= storiesPerPage)
                    {
                        return result;
                    }
                }
            }
            //This means we couldn't fill the page with stories
            return result;
        }

        private static async Task<List<long>> GetTopStoryIds(){
            string content;
            using (HttpResponseMessage httpResponseMessage = await client.GetAsync(BASE_NEWS_URL + NEW_NEWS_PATH))
            {
                if (!httpResponseMessage.IsSuccessStatusCode)
                {
                    throw new HackerNewsException(httpResponseMessage.ReasonPhrase);
                }
                content = await httpResponseMessage.Content.ReadAsStringAsync();
            }
            List<long> itemIds = JsonConvert.DeserializeObject<List<long>>(content);

            return itemIds;
        }

        private static async Task<Item> GetItemAsync(long id)
        {
            using (HttpResponseMessage httpResponseMessage = await client.GetAsync(BASE_NEWS_URL + ITEM_PATH + id + ".json"))
            {
                if (!httpResponseMessage.IsSuccessStatusCode)
                {
                    throw new HackerNewsException(httpResponseMessage.ReasonPhrase);
                }
                string content = await httpResponseMessage.Content.ReadAsStringAsync();
                
                return JsonConvert.DeserializeObject<Item>(content);
            }
        }
    }
    public class NewsOverview
    {
        public readonly string headline;
        public readonly string author;
        public readonly string url;
        public NewsOverview(string headline, string author, string url)
        {
            this.headline = headline;
            this.author = author;
            this.url = url;
        }
    }
}
