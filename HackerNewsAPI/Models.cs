﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace HackerNewsFeed.HackerNewsAPI
{
    public class Item
    {
        //Only including relevant fields
        public long id { get; set; }
        public Boolean deleted{ get; set; }
        public ItemType type{ get; set; }
        public string by{ get; set; }
        public long time { get; set; }
        public string title { get; set; }
        public string url { get; set; }
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum ItemType
    {
        [EnumMember(Value="job")]
        job,
        [EnumMember(Value = "story")]
        story,
        [EnumMember(Value = "comment")]
        comment,
        [EnumMember(Value = "poll")]
        poll,
        [EnumMember(Value = "pollopt")]
        pollopt
    }
}
