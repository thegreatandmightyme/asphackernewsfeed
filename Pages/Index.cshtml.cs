﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace HackerNewsFeed.Pages
{
    public class IndexModel : PageModel
    {

        public readonly string PAGE_NUMBER_NAME = "page";
        public int pageNumber { get; private set; } = 0;
        public readonly string RESULTS_PER_PAGE_NAME = "resultsPerPage";
        public int resultsPerPage { get; private set; } = 20;
        public readonly string SEARCH_NAME = "search";
        public string search { get; private set; } = null;

        public void OnGet()
        {
            if (Request.Query.ContainsKey(PAGE_NUMBER_NAME))
            {
                //If there are multiple page numbers, just use the first
                //If the page number doesn't parse, it will be set to 0
                int.TryParse(Request.Query[PAGE_NUMBER_NAME].First(), out int newPageNumber);
                //No negative pages
                pageNumber = Math.Max(0, newPageNumber);
            }
            if (Request.Query.ContainsKey(RESULTS_PER_PAGE_NAME))
            {
                //If there are multiple page numbers, just use the first
                if(int.TryParse(Request.Query[RESULTS_PER_PAGE_NAME].First(), out int newResultsPerPage))
                {
                    //Only set this if the parse worked, otherwise it will be 0
                    //Minimum results per page: 1
                    resultsPerPage = Math.Max(1, newResultsPerPage);
                }
            }
            if(Request.Query.ContainsKey(SEARCH_NAME))
            {
                search = Request.Query[SEARCH_NAME];
            }
        }

        public bool isFirstPage()
        {
            return pageNumber == 0;
        }

        public string getQueryString(params (string key, string value)[] alterations){

            //Build a query string with the current settings
            NameValueCollection queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);
            queryString[PAGE_NUMBER_NAME] = pageNumber.ToString();
            queryString[RESULTS_PER_PAGE_NAME] = resultsPerPage.ToString();
            if (search != null && search.Length > 0)
            {
                queryString[SEARCH_NAME] = search;
            }

            //Make sure noone explicitly passed null
            if (alterations != null)
            {
                foreach ((string key, string value) alteration in alterations)
                {
                    queryString[alteration.key] = alteration.value;
                }
            }

            return queryString.ToString();
        }
}
}
