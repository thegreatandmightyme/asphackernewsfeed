﻿using Xunit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HackerNewsFeed.HackerNewsAPI;
using System.Reflection;

namespace HackerNewsFeed.Tests
{
    public class HackerNewAPITests
    {
        [Theory]
        [InlineData(20)]
        [InlineData(1)]
        [InlineData(21)]
        public void TestFullFetch(int storiesToFetch)
        {
            Task<List<NewsOverview>> task = HackerNewsAPI.HackerNewsAPI.GetNewStories(storiesToFetch);
            task.Wait(5000);
            Assert.Equal(task.Result.Count, storiesToFetch);
        }

        [Theory]
        [InlineData(5, 7)]
        [InlineData(6, 2)]
        [InlineData(25, 2)]
        public void TestPagingFetch(int storiesPerPage, int page)
        {
            Task<List<NewsOverview>> task = HackerNewsAPI.HackerNewsAPI.GetNewStories(storiesPerPage, page);
            task.Wait(5000);
            Assert.Equal(task.Result.Count, storiesPerPage);
        }

        //Super easy search conditions to keep this quick
        [Theory]
        [InlineData(5, 3, "to")]
        [InlineData(6, 1, "to")]
        [InlineData(25, 0, "f")]
        public void TestSearch(int storiesPerPage, int page, string search)
        {
            Task<List<NewsOverview>> task = HackerNewsAPI.HackerNewsAPI.GetNewStories(storiesPerPage, page, search);
            task.Wait(5000);
            Assert.All<NewsOverview>(task.Result, delegate (NewsOverview newsOverview)
            {
                newsOverview.headline.Contains(search);
            });
        }

        [Fact]
        public void TestItemFetch()
        {
            HackerNewsAPI.HackerNewsAPI hackerNewsAPI = new HackerNewsAPI.HackerNewsAPI();
            Type tipe = hackerNewsAPI.GetType();

            MethodInfo method = tipe.GetMethod("GetItemAsync", BindingFlags.NonPublic | BindingFlags.Static);
            Task<Item> task = (Task<Item>)method.Invoke(null, new Object[] { 8863 }); //ID used in the hacker news docs
            task.Wait(5000);
            Item item = task.Result;

            Assert.NotNull(item);
            Assert.Equal("dhouston", item.by);
            Assert.Equal(8863, item.id);
            Assert.Equal(ItemType.story, item.type);
        }

        [Fact]
        public void TestTopStoryFetch()
        {
            HackerNewsAPI.HackerNewsAPI hackerNewsAPI = new HackerNewsAPI.HackerNewsAPI();
            Type tipe = hackerNewsAPI.GetType();

            MethodInfo method = tipe.GetMethod("GetTopStoryIds", BindingFlags.NonPublic | BindingFlags.Static);
            Task<List<long>> task = (Task<List<long>>)method.Invoke(null, null);
            task.Wait(5000);
            List<long> ids = task.Result;

            Assert.NotNull(ids);
            Assert.NotEmpty(ids);
        }
    }
}
